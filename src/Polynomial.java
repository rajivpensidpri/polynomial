import java.util.* ; 
public class Polynomial {
	
	public TreeMap<Integer,Double> terms ; 
	
	public enum NATURE {
		INCREASING, DECREASING,LOCAL_MAX,LOCAL_MIN ; 
	}
	
	public Polynomial(String s) {
		
		String[] splits = s.split(" ") ; 
		terms = new TreeMap<>() ; 
		for(int i = 0  ; i < splits.length ; i+=2) {
			int key = Integer.parseInt(splits[i]) ; 
			double value = Double.parseDouble(splits[1]) ; 
			terms.put(key, value) ; 
		}
		
	}
	public Polynomial () {
		terms = new TreeMap<>() ; 
	}
	public Polynomial(TreeMap<Integer,Double> newTerms) {
		this.terms = newTerms ; 
	}
	
	public void add(Polynomial p) {
	    
        Map<Integer, Double> tm = this.terms;    
        p.terms.forEach((k, v) -> tm.merge(k, v, Double::sum));
        this.terms = (TreeMap<Integer, Double>) tm ; 
    }
	
	public void subtract(Polynomial p) {
        TreeMap<Integer,Double> result = new TreeMap<>() ; 
        p.scalarmult(-1);
        this.add(p); 
    }
	
	public void multiply(Polynomial p) {
		TreeMap<Integer,Double> product = new TreeMap<>() ; 
		
		for (Map.Entry<Integer, Double> e1 : this.terms.entrySet()) {
			for(Map.Entry<Integer, Double> e2 : p.terms.entrySet()) {
				int exp = e1.getKey() + e2.getKey() ; 
				double coeff = e1.getValue() + e2.getValue() ; 
				if (product.containsKey(exp)) {
					product.put(exp,	coeff + product.get(exp)) ; 
				}
				else {
					product.put(exp,	coeff) ; 
				}
			}
		}
		this.terms = product ; /////////
		
		
	}
	public Polynomial differentiate(Polynomial p) {
	    TreeMap<Integer,Double> diffP = new TreeMap<>();
		
	    for(Map.Entry<Integer,Double> entry: p.terms.entrySet()){
		        double d = (double)entry.getKey()*entry.getValue();
		        diffP.put(entry.getKey()-1,d);
		}
	    return new Polynomial(diffP) ; 
	}
	
	public Polynomial integrate() {
	    TreeMap<Integer,Double> intgP =new TreeMap<>();
		for(Map.Entry<Integer,Double> entry: this.terms.entrySet()){
		        double d = entry.getValue()/((double)entry.getKey()+1.0);
		        intgP.put(entry.getKey()+1,d);
		}
	   return new Polynomial(intgP) ; 
	}

	public double valueAt(double x) {
		double valueAtX = 0.0;
		for(Map.Entry<Integer,Double> entry: this.terms.entrySet()){
		    valueAtX +=  entry.getValue()* Math.pow(x,entry.getKey());
		}
		return valueAtX;
	}
	public void scalarmult(int k) {
		for(Map.Entry<Integer, Double> e : this.terms.entrySet()) {
			this.terms.put(e.getKey(), k*e.getValue()) ; 
		}
		
	}

	public NATURE getNatureAt(double x) {
		double firstDifferential = differentiate(this).valueAt(x) ; 
		if (firstDifferential == 0) {
			double secondDifferential = differentiate(differentiate(this)).valueAt(x) ; 
			if (secondDifferential > 0) {
				return NATURE.LOCAL_MIN ; 
			}
			return NATURE.LOCAL_MAX ; 
		}
		if (firstDifferential > 0) {
			return NATURE.INCREASING ; 
		}
		return NATURE.DECREASING ; 
	}
	private void consolidate(){
	    for(Map.Entry<Integer,Double> entry: this.terms.entrySet()){
		    if(entry.getValue() == 0.0){
		        this.terms.remove(entry.getKey());
		    }
		} 
	}
	
	@Override 
	public String toString() {
		String ret = "" ; 
		for(Map.Entry<Integer, Double> e : this.terms.entrySet()) {
			ret += e.getValue() + " x^" + e.getKey() + " " ; 
		}
		return ret ; 
	}
	
	
}
